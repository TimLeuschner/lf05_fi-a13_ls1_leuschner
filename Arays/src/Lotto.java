
public class Lotto {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// int-Array mit 6 Lottozahlen erstellen:
		int[] LottoZahlen = new int[6];
		
		// Zahlen 3, 7, 12, 18, 37 und 42 eingeben.
				LottoZahlen[0] = 3;
				LottoZahlen[1] = 7;
				LottoZahlen[2] = 12;
				LottoZahlen[3] = 18;
				LottoZahlen[4] = 37;
				LottoZahlen[5] = 42;
				
				//Teil a)
				// Array ausgeben.
				// [  3  7  12  18  37  42  ]
				System.out.print("[  ");
				for(int i = 0; i < LottoZahlen.length; i++) {
					System.out.print(LottoZahlen[i] + "  ");
				}		
				System.out.println("]");
				
				//Teil b)
				
				
				
				int gesuchteZahl;
				boolean istEnthalten;
				
				gesuchteZahl = 12;
				istEnthalten = false;
				for(int i = 0; i < LottoZahlen.length; i++) {
				   if(LottoZahlen[i] == gesuchteZahl) {
					   istEnthalten = true;
					   break;
				   }
				}
				if(istEnthalten) {
					   System.out.println("Die Zahl " + gesuchteZahl +  " ist in der Ziehung enthalten.");			
				}
				else {
					   System.out.println("Die Zahl " + gesuchteZahl +  " ist nicht in der Ziehung enthalten.");						
				}
				
				gesuchteZahl = 13;
				istEnthalten = false;
				for(int i = 0; i < LottoZahlen.length; i++) {
				   if(LottoZahlen[i] == gesuchteZahl) {
					   istEnthalten = true;
					   break;
				   }
				}
				if(istEnthalten) {
					System.out.println("Die Zahl " + gesuchteZahl +  " ist in der Ziehung enthalten.");			
				}
				else {
					System.out.println("Die Zahl " + gesuchteZahl +  " ist nicht in der Ziehung enthalten.");						
				}
			}
		


	}



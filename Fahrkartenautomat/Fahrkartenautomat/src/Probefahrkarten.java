import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       int anzahlTickets;
       double ticketEinzelpreis;
       
       
       System.out.print("Preis pro Ticket in (EURO): ");
       ticketEinzelpreis = tastatur.nextDouble();
       
       System.out.print("Wie viele Tickets werden gekauft?: ");
       anzahlTickets = tastatur.nextInt();
       
       zuZahlenderBetrag = anzahlTickets * ticketEinzelpreis;

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
        //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
        System.out.printf("%s%.2f%s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag)," EURO ");
        System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
        eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrscheine werden ausgegeben.");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
   Thread.sleep(250);
  } catch (InterruptedException e) {
   // TODO Auto-generated catch block
   e.printStackTrace();
  }
       }
       System.out.println("\n\n");

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(r�ckgabebetrag > 0.0)
       {
        System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
        System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
           System.out.println("2 EURO");
           r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
           System.out.println("1 EURO");
           r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
           System.out.println("50 CENT");
           r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
           System.out.println("20 CENT");
            r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
           System.out.println("10 CENT");
           r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
           System.out.println("5 CENT");
            r�ckgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}

/* Als Variablen werden double benutzt um einen genauen Betrag gerauszugeben und Integer
 * um eine ganzzahlige Ticketanzahl zu bestimmen
 */

/* Der Datentyp muss ein Integer sein, da es nur ganze Tickets gibt
 * denn es macht zum Beispiel keinen Sinn ein dreiviertel Tickets zu kaufen

* in Zeile 23 wird "anzahlTickets" mit "ticketEinzelpreis" 
* multipliziert, dadurch ist das Ergebnis "zuZahlenderBetrag" 
* vorlaeufig eine Gleitkommazahl, da "ticketEinzelpreis" eine Gleitkommazahl ist
* und somit auch zu einer Gleitkommazahl wird 
*/

// jfdhf
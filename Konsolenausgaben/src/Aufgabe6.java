
public class Aufgabe6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Variablen
		
		String a ="|"; 
		String b ="Fahrenheit"; 
		String c ="Celsius"; 
		String d ="------------------------"; 
		String e ="-20";
		Double f =-28.8889;
		String g ="-10";
		Double h =-23.3333;
		String i ="+0";
		Double j = -17.7778;
		String k ="+20";
		Double l =-6.6667;
		String m ="+30";
		Double n =-1.1111;
		
		
		// Zeile 1
		
		System.out.printf( "%-12s", b );
		System.out.printf( "%-2s",a );
		System.out.printf( "%10s\n",c );
		
		// Zeile 2
		
		System.out.printf( "%-12s\n", d );
		
		// Zeile 3
		
		System.out.printf( "%-12s", e );
		System.out.printf( "%-2s",a );
		System.out.printf( "%10.2f\n",f );
		
		// Zeile 4
		
		System.out.printf( "%-12s", g );
		System.out.printf( "%-2s",a );
		System.out.printf( "%10.2f\n",h );
		
		// Zeile 5
		
		System.out.printf( "%-12s", i );
		System.out.printf( "%-2s",a );
		System.out.printf( "%10.2f\n",j );
		
		// Zeile 6
		
		System.out.printf( "%-12s", k );
		System.out.printf( "%-2s",a );
		System.out.printf( "%10.2f\n",l );
		
		// Zeile 7
		
		System.out.printf( "%-12s", m );
		System.out.printf( "%-2s",a );
		System.out.printf( "%10.2f\n",n );

	}

}

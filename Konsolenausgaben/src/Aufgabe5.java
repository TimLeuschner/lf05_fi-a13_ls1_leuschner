
public class Aufgabe5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String a = "0!";
		String b = "=";
		String c = "1";
		String d = "";
		String e = "1!";
		String f = "2!";
		String g = "1 * 2";
		String h = "2";
		String i = "3!";
		String j = "1 * 2 * 3";
		String k = "6";
		String l = "4!";
		String m = "1 * 2 * 3 * 4";
		String n = "24";
		String o = "5!";
		String p = "1 * 2 * 3 * 4 * 5";
		String q = "120";
		
		
		//Zeile 1
		
		System.out.printf( "%-5s", a );
		System.out.printf( "%-2s",b );
		System.out.printf( "%-19s",d );
		System.out.printf( "%-1s",b );
		System.out.printf( "%4s\n",c );
		
		
		// Zeile 2
		
		System.out.printf( "%-5s", e );
		System.out.printf( "%-2s",b );
		System.out.printf( "%-19s",c );
		System.out.printf( "%-1s",b );
		System.out.printf( "%4s\n",c );
		
		// Zeile 3
		
		System.out.printf( "%-5s", f );
		System.out.printf( "%-2s",b );
		System.out.printf( "%-19s",g );
		System.out.printf( "%-1s",b );
		System.out.printf( "%4s\n",h );
		
		// Zeile 4
		
		System.out.printf( "%-5s", i );
		System.out.printf( "%-2s",b );
		System.out.printf( "%-19s",j );
		System.out.printf( "%-1s",b );
		System.out.printf( "%4s\n",k );
		
		// Zeile 5
		
		System.out.printf( "%-5s",l );
		System.out.printf( "%-2s",b );
		System.out.printf( "%-19s",m );
		System.out.printf( "%-1s",b );
		System.out.printf( "%4s\n",n );
		
		// Zeile 6
		
		System.out.printf( "%-5s",o );
		System.out.printf( "%-2s",b );
		System.out.printf( "%-19s",p );
		System.out.printf( "%-1s",b );
		System.out.printf( "%4s\n",q );
	}

}
